import { Component }        from '@angular/core';
import { AnalyticsService } from './analytics.service';

@Component({
  selector: 'demo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public now = new Date();

  constructor(analyticsService: AnalyticsService) {
    analyticsService.init();
  }
}
