import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent }        from './home/home.component';

const routes: Routes = [
  { path: '', data: { seo: { title: '@ripp/SEO - SEO management', description: 'Easily manage SEO and Social Media meta data programmatically'} }, component: HomeComponent },
  { path: 'sandbox', data: { seo: { title: 'Sandbox', description: 'Hands-on experience to view produced tags'} }, loadChildren: () => import('./sandbox/sandbox.module').then(m => m.SandboxModule) },
  { path: 'docs', data: { seo: { title: 'Documentation', description: '@ripp/seo documentation'} }, loadChildren: () => import('./docs/docs.module').then(m => m.DocsModule) },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
