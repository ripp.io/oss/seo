import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  trackingId = 'G-MV389TGWJC';
  initialized = false;
  gtag: any;

  constructor(public router: Router) {
    if (typeof window !== 'undefined') {
      this.gtag = (window as any).gtag
    }
  }

  init() {
    if (typeof window === 'undefined' || this.initialized) {
      return;
    }
    this.initialized = true;

    this.gtag('create', this.trackingId, 'auto');

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.pushPageView(event.urlAfterRedirects);
      }
    });
  }

  pushPageView(url: string) {
    if (this.initialized) {
      let params = {
        page_path:     url,
        page_title:    'SEO',
        page_location: window.location.href
      }
      this.gtag('config', this.trackingId, params);
    }
  }

  emitEvent(action: string,
            event_category: string,
            event_label: string,
            event_value: number) {

    if (this.initialized) {
      this.gtag('event', action, { event_category, event_label, event_value });
    }
  }
}
