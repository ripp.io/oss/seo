import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SandboxRoutingModule } from './sandbox-routing.module';
import { SandboxComponent }     from './sandbox.component';
import { MatButtonModule }      from '@angular/material/button';


@NgModule({
  declarations: [
    SandboxComponent
  ],
  imports: [
    CommonModule,
    SandboxRoutingModule,
    MatButtonModule
  ]
})
export class SandboxModule { }
