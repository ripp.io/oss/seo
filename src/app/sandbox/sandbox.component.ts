import { Component, Inject }             from '@angular/core';
import { SEO_META_DEFAULTS, SeoManager } from '@ripp/seo';
import { DOCUMENT }                      from '@angular/common';

@Component({
  selector:    'demo-sandbox',
  templateUrl: './sandbox.component.html',
  styleUrls:   ['./sandbox.component.scss']
})
export class SandboxComponent {
  seoManager: SeoManager | undefined;
  defaults = JSON.stringify(SEO_META_DEFAULTS, null, 2);
  page1    = '{\n  "title": "Page 1",\n  "description": "The first example page"\n}';
  page1Parsed: any;
  page2    = '{\n  "title": "Page 2",\n  "description": "Another example page"\n}';
  page2Parsed: any;
  output   = '';

  defaultError = '';
  page1Error   = '';
  page2Error   = '';

  defaultTimeout: any;
  page1Timeout: any;
  page2Timeout: any;
  timeOutDuration = 500;

  head: HTMLHeadElement;
  fakeDocument: any;

  constructor(@Inject(DOCUMENT) private readonly document: Document) {
    this.head         = this.document.createElement('head');
    this.fakeDocument = {
      head:          this.head,
      createElement: (el: any) => this.document.createElement(el),
      location:      {
        href:     "https://example.com/",
        search:   '',
        pathname: "/"
      }
    };

    this.seoManager = new SeoManager({
      document: this.fakeDocument
    });
    this.updateOutput();
  }

  updateOutput() {
    this.output = this.head.innerHTML.replace(/</g, "\n<").replace("\n</title", "</title").substr(1)
  }

  updateDefault(defaults: string) {
    clearTimeout(this.defaultTimeout);
    this.defaultTimeout = setTimeout(() => {
      try {
        const parsed      = JSON.parse(defaults);
        this.defaultError = '';
        this.seoManager   = new SeoManager({
          defaults: parsed,
          document: this.fakeDocument
        });
        this.updateOutput();
      } catch (e) {
        this.defaultError = 'Invalid JSON';
      }
    }, this.timeOutDuration);
  }

  updatePage1(page1: string) {
    clearTimeout(this.page1Timeout);
    this.page1Timeout = setTimeout(() => {
      try {
        this.page1Parsed = JSON.parse(page1.replace(/([a-zA-Z0-9-]+):([a-zA-Z0-9-]+)/g, "\"$1\":\"$2\""));
        this.page1Error = '';
      } catch (e) {
        this.page1Error = 'Invalid JSON';
      }
    }, this.timeOutDuration);
  }

  updatePage2(page2: string) {
    clearTimeout(this.page2Timeout);
    this.page2Timeout = setTimeout(() => {
      try {
        this.page2Parsed = JSON.parse(page2);
        this.page2Error = '';
      } catch (e) {
        this.page2Error = 'Invalid JSON';
      }
    }, this.timeOutDuration);
  }

  navigate(pageContext: any, pageName: string) {
    this.fakeDocument.location.href     = 'https://example.com/' + pageName;
    this.fakeDocument.location.pathname = '/' + pageName;
    this.seoManager!.update(pageContext);
    this.updateOutput();
  }
}
