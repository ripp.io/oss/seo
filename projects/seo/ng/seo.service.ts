import { Inject, Injectable }                    from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription }                          from 'rxjs';
import { DOCUMENT }                              from '@angular/common';
import { filter, map }                           from 'rxjs/operators';
import { SeoConfig, SeoContext, SeoManager }     from '@ripp/seo';
import { Meta, Title }                           from '@angular/platform-browser';

export class SeoServiceConfig extends SeoConfig {
  listenToRoute?: boolean
}

const DEFAULTS: SeoServiceConfig = {
  listenToRoute: true,
};

@Injectable({ providedIn: 'root' })
export class SeoService {
  private seoManager?: SeoManager;
  private routeSub?: Subscription;

  constructor(
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly title: Title,
    private readonly meta: Meta,
    @Inject(DOCUMENT) private readonly document: Document,
  ) {}

  initialize(config?: SeoServiceConfig): SeoService {
    config = {
      ...DEFAULTS,
      ...{
        document:     this.document,
        titleHandler: title => this.title.setTitle(title),
        metaHandler:  attrs => this.meta.updateTag(attrs),
      },
      ...config
    };
    this.seoManager = new SeoManager(config);

    if (config?.listenToRoute) {
      this.registerRouter();
    }
    return this;
  }

  registerRouter(): SeoService {
    if (this.routeSub) {
      return this; // Already registered
    }

    this.routeSub = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter(route => route.outlet === 'primary')
    ).subscribe((route: ActivatedRoute) => {
      // Get data from `route.data.seo` OR fallback to static `component.SEO` object
      const context = route.snapshot.data.seo || (route.component as any)?.SEO;
      this.update(context);
    });
    return this;
  }

  update(context?: SeoContext): SeoService {
    this.seoManager?.update(context);
    return this;
  }
}
