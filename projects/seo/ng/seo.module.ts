import { ModuleWithProviders, NgModule, Optional } from '@angular/core';
import { SeoService, SeoServiceConfig }            from './seo.service';

@NgModule({
  declarations: [],
  imports:      [],
  exports:      []
})
export class SeoModule {
  constructor(seoService: SeoService, @Optional() serviceConfig?: SeoServiceConfig) {
    seoService.initialize(serviceConfig);
  }

  static withConfig(serviceConfig: SeoServiceConfig): ModuleWithProviders<SeoModule> {
    return {
      ngModule:  SeoModule,
      providers: [{ provide: SeoServiceConfig, useValue: serviceConfig }],
    };
  }
}
