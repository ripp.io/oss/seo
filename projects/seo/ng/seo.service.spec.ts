import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { SeoService } from './seo.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Component } from '@angular/core';

const dataExample = { title: 'Data Example' };
const staticExample = { title: 'Static Example' };
const manualExample = { title: 'Manual Example' };

@Component({
  selector: 'ripp-static',
  template: ''
})
class SeoStaticComponent {
  static SEO = staticExample;
}
@Component({
  selector: 'ripp-data',
  template: ''
})
class SeoDataComponent {}
@Component({
  selector: 'ripp-manual',
  template: ''
})
class SeoManualComponent {}

describe('SeoService', () => {
  let service: SeoService;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        SeoStaticComponent,
        SeoDataComponent,
        SeoManualComponent,
      ],
      imports: [
        RouterTestingModule.withRoutes([
          { path: '', component: SeoManualComponent }, // Reusing manual for blank -- nothing tied to it
          { path: 'static', component: SeoStaticComponent },
          { path: 'data', component: SeoDataComponent, data: { seo: dataExample } },
          { path: 'manual', component: SeoManualComponent },
        ])
      ]
    });
    service = TestBed.inject(SeoService);
    router = TestBed.inject(Router);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
    // @ts-ignore
    expect(service.seoManager).toBeFalsy();
    // @ts-ignore
    expect(service.routerSub).toBeFalsy();

  });

  describe('initialization', () => {
    it('should initialize with defaults', fakeAsync(() => {
      service.initialize({ urlMapping: { '/manual': manualExample }});
      // @ts-ignore
      expect(service.seoManager).toBeTruthy();
      // @ts-ignore
      const routeSub = service.routeSub;
      expect(routeSub).toBeTruthy();

      // Should shortcut and not double register
      service.registerRouter();

      const updateSpy        = spyOn(service, 'update');

      // Root has no SEO settings
      router.navigate(['/']);
      tick();
      expect(updateSpy).toHaveBeenCalledTimes(1);
      expect(updateSpy).toHaveBeenCalledWith(undefined); // No manual, data, or static SEO found

      // Get from Static SEO Field on component
      router.navigate(['/static']);
      tick();
      expect(updateSpy).toHaveBeenCalledTimes(2);
      expect(updateSpy).toHaveBeenCalledWith(staticExample);

      // Get from data on route config
      router.navigate(['/data']);
      tick();
      expect(updateSpy).toHaveBeenCalledTimes(3);
      expect(updateSpy).toHaveBeenCalledWith(dataExample);

      // Get from matching path in manual config
      router.navigate(['/manual']);
      tick();
      expect(updateSpy).toHaveBeenCalledTimes(4);
      expect(updateSpy).toHaveBeenCalledWith(undefined); // No path/static - will be pulled via document.location.pathname

      routeSub?.unsubscribe(); // Cleanup
    }));

    it('should not listen to routes', fakeAsync(() => {
      service.initialize({ listenToRoute: false });
      // @ts-ignore
      expect(service.routeSub).toBeFalsy();

      const updateSpy        = spyOn(service, 'update');
      router.navigate(['/']);
      tick();
      expect(updateSpy).toHaveBeenCalledTimes(0);
    }));

    it('should send onto manager', fakeAsync(() => {
      service.initialize({ listenToRoute: false });
      // @ts-ignore
      const manager: SeoManager = service.seoManager;
      const updateSpy = spyOn(manager, 'update');
      const meta = { test: 'stuff' };

      service.update(meta);

      expect(updateSpy).toHaveBeenCalledTimes(1);
      expect(updateSpy).toHaveBeenCalledWith(meta);
    }));
  });
});
