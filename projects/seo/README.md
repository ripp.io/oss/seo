# @ripp/SEO

Programmatically manage SEO, Social Sharing meta, and other header elements with ease.

Provide updates in the form of a simple context object to drive multiple elements.

**Execute:**
```typescript
seoManager.update({ title: 'Cool Page' });
```
**Produces:** (DOM)
```html
<title>Cool Page</title>
<meta property="og:title" content="Cool Page"></meta>
<meta property="twitter:title" content="Cool Page"></meta>
```

Elements will be added/updated/removed as needed and ***is Server-Side-Rendering Ready***.

Comes with many mappings out-of-the-box and is extensible/overridable.

- [Quick Start](#quick-start)
- [Usage](#usage)
- [Angular](#angular)
- [Default Mappings](#default-mappings)
- [Configuration](#configuration)
  - [Defaults](#defaults)
  - [Mappings](#mappings)
  - [URL Mappings](#url-mappings)
  - [Document](#document)
- [SEO Best Practices](#best-practices)
- [Future Features](#future-features)
- [Contributing](#contributing)
  - [Code](#contributing-code)
  - [Feature Requests, Feedback, or Issues](#feature-request-feedback-or-issues)
- [License](#license)

## Quick Start

> For Angular, see [Angular Section](#angular)

```bash
npm i -S @ripp/seo
```

**No configuration required**:
```typescript
import { SeoManager } from '@ripp/seo';

const seoManager = new SeoManager();
```

---

**Provide configuration**:

```typescript
import { SeoManager }          from '@ripp/seo';
import { SEO_DEFAULTS }        from './seo.defaults';
import { SeoUrlContextMapper } from './seo.types';

const seoConfig: SeoConfig = {
  // Set manager configurations (see `SeoConfig` for more)
  hrefAsCanonical: true,
  
  // Supply Defaults
  defaults: {
    title:  'Cool Site',
    isCool: true,
  },
  
  // Recommended to extend SEO_META_MAPPING_DEFAULT but can be completely overridden
  mappings: Object.assign({}, SEO_META_MAPPING_DEFAULT, {
    cool: {
      tag:   'meta',
      attrs: {
        name:    '!',
        content: '${isCool}' // Template via context - see default
      }
    },
  }),

  // Manually provide mappings of URL path to context to load - note this uses `document.location.pathname` and requires leading '/'
  urlMapper: {
    '/my/cool/url': {
      isCool: 'Yeah, but manual mapping not recommended'
    }
  }
}

const seoManager = new SeoManager(seoConfig);
```

> See [Configuration](#configuration) for more options.

## Usage

```typescript
const newContext: SeoContext = {
  title: 'A Cool Page in this Cool Site',
  isCool: 'Most Definitely!'
};

seoManager.update(newContext);
```

Usage is simply calling `seoManager.update(context)` when a change in SEO should be done, likely upon page navigation.

A [URL Mapper](#url-mappings) can be supplied, but recommended to load SEO objects as needed to reduce file sizes.

## Angular

```bash
ng add @ripp/seo
```
> Schematic optionally registers SeoModule with root module

**Register and Configure**:

```typescript
import { SeoModule } from '@ripp/seo/ng';

...
@NgModule({
  ...
  imports: [
    ...
    SeoModule,
    
    // OR with Configuration
    SeoModule.withConfig({
      listenToRoute: true,
      defaults:      {},
      mappings:      {
        ...SEO_META_MAPPING_DEFAULT,
        ...{}
      },
      urlMapping:    {},
    }),
  ],
...
export class AppModule { }
```

Registering the `SeoModule` will execute `SeoService.initialize(config)` which will:
- Create the `SeoManager`,
- Set initial SEO elements
- Optionally (default) register a router events listener to call `SeoService.update(...)` on route changes with:
  - Router entry `data.seo` object found on activated route
    - Example: `{ path: 'my-page', component: MyPageComponent, data: { seo: { title: 'My Page' } } }`
  - Static object `SEO` on component being loaded
    - Example:
      ```
      @Component({
        ...
      })
      class MyPageComponent {
        static SEO = { title: 'My Page' };
      }
      ```
      
`SeoService` is also available to call `update()` on directly.

> Note that [Manually mapped URL Contexts](#url-mappings) will still be evaluated and merged in

See [Usage](#usage) for more.

## Default Mappings

| Context (Driver) | SEO Element(s) Affected |
| :--- | :--- |
| languageAlternateHref | `<link rel=alternate href=$ hreflang=$></link>` |
| languageAlternate | `<link rel=alternate href=$ hreflang=$></link>` |
| viewport | `<meta name=viewport content=$></meta>` |
| title | `<title>$</title>`<br>`<meta property=og:title content=$></meta>`<br>`<meta property=twitter:title content=$></meta>` |
| icon | `<link rel=icon href=$ type=image/x-icon></link>` |
| canonical | `<link rel=canonical href=$></link>`<br>`<meta property=og:url content=$></meta>`<br>`<meta property=twitter:url content=$></meta>` |
| description | `<meta name=description content=$></meta>`<br>`<meta property=og:description content=$></meta>`<br>`<meta property=twitter:description content=$></meta>` |
| robots | `<meta name=robots content=$></meta>` |
| keywords | `<meta name=keywords content=$></meta>` |
| author | `<meta name=author content=$></meta>` |
| copyright | `<meta name=copyright content=$></meta>` |
| facebookAppId | `<meta property=fb:app_id content=$></meta>` |
| facebookAdmins | `<meta property=fb:admins content=$></meta>` |
| siteName | `<meta property=og:site_name content=$></meta>` |
| type | `<meta property=og:type content=$></meta>` |
| logo | `<meta property=og:logo content=$></meta>` |
| image | `<meta property=og:image content=$></meta>`<br>`<meta property=twitter:image content=$></meta>` |
| imageAlt | `<meta property=og:image:alt content=$></meta>`<br>`<meta property=twitter:image:alt content=$></meta>` |
| video | `<meta property=og:video content=$></meta>` |
| videoAlt | `<meta property=og:video:alt content=$></meta>` |
| audio | `<meta property=og:audio content=$></meta>` |
| audioAlt | `<meta property=og:audio:alt content=$></meta>` |
| locale | `<meta property=og:locale content=$></meta>` |
| localeAlt | `<meta property=og:locale:alternate content=$></meta>` |
| twitterCard | `<meta property=twitter:card content=$></meta>` |
| twitterSite | `<meta property=twitter:site content=$></meta>` |
| twitterCreator | `<meta property=twitter:creator content=$></meta>` |
| player | `<meta property=twitter:player content=$></meta>` |
| stream | `<meta property=twitter:stream content=$></meta>` |
| iphoneAppName | `<meta property=twitter:app:name:iphone content=$></meta>` |
| iphoneAppId | `<meta property=twitter:app:id:iphone content=$></meta>` |
| iphoneAppUrl | `<meta property=twitter:app:url:iphone content=$></meta>` |
| ipadAppName | `<meta property=twitter:app:name:ipad content=$></meta>` |
| ipadAppId | `<meta property=twitter:app:id:ipad content=$></meta>` |
| ipadAppUrl | `<meta property=twitter:app:url:ipad content=$></meta>` |
| googleplayAppId | `<meta property=twitter:app:name:googleplay content=$></meta>` |
| googleplayAppName | `<meta property=twitter:app:id:googleplay content=$></meta>` |
| googleplayAppUrl | `<meta property=twitter:app:url:googleplay content=$></meta>` |
| contentAuthor | `<meta property=article:author content=$></meta>` |
| contentPublisher | `<meta property=article:publisher content=$></meta>` |
| contentSection | `<meta property=article:section content=$></meta>` |
| contentTags | `<meta property=article:tag content=$></meta>` |
| contentPublishedAt | `<meta property=article:published_time content=$></meta>` |
| contentModifiedAt | `<meta property=article:modified_time content=$></meta>` |
| contentExpiresAt | `<meta property=article:expiration_time content=$></meta>` |

## Configuration

See [`seo.types.ts::SeoConfig`](src/seo.types.ts#L430) for self-documented options.

Important configurations:
- [Defaults](#defaults) - Supply base SEO Context defaults to always be applied
- [Mappings](#mappings) - Override SEO Element Mappings
- [URL Mappings](#url-mappings) - Provide SEO Contexts to be applied when a `document.location.pathname` matches
- [Title Handler](#title-handler) - execute when <title> tag is to be updated, providing updated title
- [Meta Handler](#meta-handler) - execute when a <meta> tag is to be updated, providing the attributes
- [Document](#document) - Supply a DOM Document - used for Server-Side-Rendering solutions such as Angular Universal

### Defaults

This `SeoContext` object is applied as a base to all calls to `update(context)`.

As an example, you can provide a standard website `title` here.
Individual pages may override it in their call to `update(context)`, but when a `title` is not supplied, the default is put back in place.

### Mappings

A hashmap of well-known identifiers to instructions on producing the element.

This consists of 2 things:
  - Tag to create (meta, link, title, etc.)
  - Attributes to set (or innerText)

Attributes can be set in 3 ways:
  - Hard-coded value
  - Self-Referencing `!` - uses hashmap's key as value
  - Templated String `${contextField}`

This hashmap is pre-processed, creating a map of 'drivers' from the context fields used in the templated strings.
When an `update(context)` call is made, this driver map is used to identify and update elements as needed.

### URL Mappings

Gives the ability to supply a manual mapping of URLs to SEO Context's to apply.
Useful if you prefer centralizing your SEO configuration but downside is a larger root module size.

For example:
```typescript
{
  '/': { title: 'My Site' },
  '/page1': { title: 'Cool Page' }
}
```
The first entry `/` would be applied when `document.location.pathname` is at the root domain.
The second would apply when navigated to `/page1`.

> Note: a call to `update()` must still be performed on navigation as there's no navigation listener (unless you're using the angular wrapper)

### Title Handler

A callback that's executed when a new title is to be updated, providing the new title.

This is mostly used for framework integration, namely Angular Universal.

### Meta Handler

A callback that's executed when a <meta> tag is to be updated, providing the attributes and rendered values. 

This is mostly used for framework integration, namely Angular Universal.

### Document

In order to support Server-Side-Rendered solutions, such as Angular Universal, a Document can be provided when there is no DOM.

This is used to:
- Find existing elements
- Create elements
- Access the current pathname

When not provided, will attempt to grab `window.document`.

## Best Practices

Recommend setting at least:
- Title
  - Each page should load a unique title
- Description
  - Each page should load a unique description
- Image
  - Set an image for social sharing and link previews
  - Supply a default so there's an image fallback for all pages
- Canonical
  - Set to the URL being loaded or if a page is reachable under multiple URLs, direct to the preferred URL
- Robots
  - SeoManager defaults to `index, follow`

A great article to understand the basics: https://ahrefs.com/blog/seo-meta-tags/

## Future Features

-Lazy fetching of SEO Configuration
  - Thinking the best option is to provide a callback/emitter/observable that provides as much information as possible
  - Allows implementation to utilize their own loading method (cache, REST, GraphQL, etc.)
- Low-level implementation of listening to navigation changes
  - Thinking optionally (false by default) listening to window's `popstate`
  - Bypass event registry when no DOM available

## Contributing

### Contributing code

- [Create a fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
- Add a feature or fix a bug
- Test!
  - `npm run test`      // Test with coverage
  - `npm run start`     // Run locally for smoke testing
  - `npm run start:ssr` // Run SSR locally for smoke testing
- Update documentation if needed
  - Note that a markdown table of mappings is produced by a test
- [Open an Merge Request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
  - Earlier the better to get feedback - remember there's a WIP indicator
- Wait for a review

#### Gotchas

- Library (`/projects/seo`) utilizes a [Secondary Entry Point](https://github.com/ng-packagr/ng-packagr/blob/master/docs/secondary-entrypoints.md) to allow core use and individual framework wrappers (Angular for example)
- Always utilize `this.document` in `SeoManager` to support [Server-Side-Rendering solutions](https://angular.io/guide/universal#working-around-the-browser-apis)

### Feature Request, Feedback or Issues

Open a Gitlab issue:
- [Feature Request](https://gitlab.com/ripp.io/oss/seo/-/issues/new?issuable_template=feature_request)
- [Issue](https://gitlab.com/ripp.io/oss/seo/-/issues/new?issuable_template=issue)
- [Feedback](https://gitlab.com/ripp.io/oss/seo/-/issues/new?issuable_template=feedback)

Always open for improvement and feedback!

## License

MIT License (see [LICENSE.txt](./LICENSE.txt))
