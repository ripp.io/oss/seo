// https://developers.facebook.com/tools/debug/
// https://cards-dev.twitter.com/validator
// https://www.linkedin.com/post-inspector/inspect/
// https://search.google.com/test/rich-results

export interface SeoContext {
  ////////////////////////////////////////////////////////////
  // Global [likely]
  ////////////////////////////////////////////////////////////
  siteName?: string;
  icon?: string;
  logo?: string;
  robots?: string;
  author?: string;
  copyright?: string;
  viewport?: string; // https://developer.mozilla.org/en-US/docs/Mozilla/Mobile/Viewport_meta_tag
  locale?: string;
  localeAlt?: string;
  languageAlternateHref?: string; // See https://moz.com/learn/seo/hreflang-tag
  languageAlternate?: string; // See https://moz.com/learn/seo/hreflang-tag

  ////////////////////////////////////////////////////////////
  // Page Info
  ////////////////////////////////////////////////////////////
  title?: string; // Recommended 60 characters max
  description?: string; // Recommended 50 - 160 characters
  canonical?: string;
  type?: string; // https://ogp.me/#types
  keywords?: string[];

  ////////////////////////////////////////////////////////////
  // Media
  ////////////////////////////////////////////////////////////
  image?: string;
  imageAlt?: string;
  audio?: string;
  audioAlt?: string;
  video?: string;
  videoAlt?: string;
  player?: string;
  stream?: string;

  ////////////////////////////////////////////////////////////
  // Social Specifics
  ////////////////////////////////////////////////////////////
  facebookAppId?: string; // 123456789
  facebookAdmins?: string[]; // ['userid1', 'userid2']
  twitterCreator?: string; // @exampleuser
  twitterSite?: string; // @examplesite

  ////////////////////////////////////////////////////////////
  // Apps
  ////////////////////////////////////////////////////////////
  iphoneAppName?: string;
  iphoneAppId?: string;
  iphoneAppUrl?: string;

  // Defaults to iphone settings
  ipadAppName?: string;
  ipadAppId?: string;
  ipadAppUrl?: string;

  googleplayAppId?: string;
  googleplayAppName?: string;
  googleplayAppUrl?: string;

  ////////////////////////////////////////////////////////////
  // Content Publishing - likely articles
  ////////////////////////////////////////////////////////////
  contentAuthor?: string;
  contentPublisher?: string;
  contentSection?: string;
  contentTags?: string[];
  contentPublishedAt?: string;
  contentModifiedAt?: string;
  contentExpiresAt?: string;

  ////////////////////////////////////////////////////////////
  // Whatever your heart desires, but please notify if missing something useful
  ////////////////////////////////////////////////////////////
  [key: string]: any;
}

export type SeoUrlContextMapper = { [url: string]: SeoContext };

// Configuration of a SEO element
export type SeoMetaTag = {
  // Type of HTML Element
  tag: string,

  // Text - used for special tags like <title>
  innerText?: string,

  // Attributes to set on element - commonly name/content or href
  // Values can use '${var}' format to pull data from current SEO `SeoContext`
  // Use '!' as value to reference metamap key
  attrs?: { [attr: string]: string }
};

// Mapping of SEO property to element config
export type SeoMetaMapping = { [key: string]: SeoMetaTag };

// SEO Manager Configuration
export class SeoConfig {
  defaults?:     SeoContext
  mappings?:     SeoMetaMapping
  urlMapping?:   SeoUrlContextMapper
  titleHandler?: (title: string) => void
  metaHandler?:  (attrs: {[attr: string]: string}) => void
  document?:     Document

  // If no canonical provided in context, use current location.href on update
  hrefAsCanonical?: boolean

  // Lookup existing DOM elements and use as defaults
  fallbackTitle?: boolean
  fallbackDescription?: boolean
  fallbackIcon?: boolean
  fallbackRobots?: boolean
  fallbackKeywords?: boolean
  fallbackAuthor?: boolean
  fallbackCopyright?: boolean
}
