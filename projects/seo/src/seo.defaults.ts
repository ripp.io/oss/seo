import { SeoContext, SeoMetaMapping } from './seo.types';

// Best-practice defaults, keyed by property for ease of customization
export const SEO_META_MAPPING_DEFAULT: SeoMetaMapping = {
  ////////////////////////////////////////////////////////////
  // Web
  ////////////////////////////////////////////////////////////
  // https://moz.com/learn/seo/hreflang-tag
  alternate: {
    tag:   'link',
    attrs: {
      rel:      '!',
      href:     '${languageAlternateHref}',
      hreflang: '${languageAlternate}'
    }
  },

  // https://developer.mozilla.org/en-US/docs/Mozilla/Mobile/Viewport_meta_tag
  viewport: {
    tag:   'meta',
    attrs: {
      name:    '!',
      content: '${viewport}'
    }
  },

  // Page Title
  title: {
    tag:       'title',
    innerText: '${title}'
  },

  // Favorite Icon
  icon: {
    tag:   'link',
    attrs: {
      rel:  '!',
      href: '${icon}',
      type: 'image/x-icon'
    }
  },

  // Page Canonical Link
  canonical: {
    tag:   'link',
    attrs: {
      rel:  '!',
      href: '${canonical}'
    }
  },

  // Page Description
  description: {
    tag:   'meta',
    attrs: {
      name:    '!',
      content: '${description}'
    }
  },

  // Page robots
  robots: {
    tag:   'meta',
    attrs: {
      name:    '!',
      content: '${robots}'
    }
  },

  // Page robots
  keywords: {
    tag:   'meta',
    attrs: {
      name:    '!',
      content: '${keywords.join(", ")}'
    }
  },

  // Page Author
  author: {
    tag:   'meta',
    attrs: {
      name:    '!',
      content: '${author}'
    }
  },

  // Site Copyright
  copyright: {
    tag:   'meta',
    attrs: {
      name:    '!',
      content: '${copyright}'
    }
  },

  ////////////////////////////////////////////////////////////
  // Facebook
  ////////////////////////////////////////////////////////////
  // App ID
  'fb:app_id': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${facebookAppId}'
    }
  },

  // Admins
  'fb:admins': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${facebookAdmins.join(", ")}'
    }
  },

  ////////////////////////////////////////////////////////////
  // Open Graph
  ////////////////////////////////////////////////////////////
  'og:site_name':   {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${siteName}'
    }
  },
  'og:type':        {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${type}'
    }
  },
  'og:url':         {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${canonical}'
    }
  },
  'og:title':       {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${title}'
    }
  },
  'og:description': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${description}'
    }
  },
  'og:logo':        {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${logo}'
    }
  },

  'og:image':     {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${image}'
    }
  },
  'og:image:alt': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${imageAlt}'
    }
  },

  'og:video':     {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${video}'
    }
  },
  'og:video:alt': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${videoAlt}'
    }
  },

  'og:audio':     {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${audio}'
    }
  },
  'og:audio:alt': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${audioAlt}'
    }
  },

  'og:locale':           {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${locale}'
    }
  },
  'og:locale:alternate': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${localeAlt}'
    }
  },

  ////////////////////////////////////////////////////////////
  // Twitter
  ////////////////////////////////////////////////////////////
  'twitter:card':        {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${twitterCard}'
    }
  },
  'twitter:site':        {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${twitterSite}'
    }
  },
  'twitter:creator':     {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${twitterCreator}'
    }
  },
  'twitter:url':         {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${canonical}'
    }
  },
  'twitter:title':       {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${title}'
    }
  },
  'twitter:description': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${description}'
    }
  },

  'twitter:image':     {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${image}'
    }
  },
  'twitter:image:alt': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${imageAlt}'
    }
  },

  'twitter:player': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${player}'
    }
  },
  'twitter:stream': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${stream}'
    }
  },

  'twitter:app:name:iphone': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${iphoneAppName}'
    }
  },
  'twitter:app:id:iphone':   {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${iphoneAppId}'
    }
  },
  'twitter:app:url:iphone':  {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${iphoneAppUrl}'
    }
  },

  'twitter:app:name:ipad': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${ipadAppName}'
    }
  },
  'twitter:app:id:ipad':   {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${ipadAppId}'
    }
  },
  'twitter:app:url:ipad':  {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${ipadAppUrl}'
    }
  },

  'twitter:app:name:googleplay': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${googleplayAppId}'
    }
  },
  'twitter:app:id:googleplay':   {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${googleplayAppName}'
    }
  },
  'twitter:app:url:googleplay':  {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${googleplayAppUrl}'
    }
  },

  ////////////////////////////////////////////////////////////
  // Article
  ////////////////////////////////////////////////////////////
  'article:author':          {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${contentAuthor}'
    }
  }, // Group/Company
  'article:publisher':       {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${contentPublisher}'
    }
  }, // http link to site
  'article:section':         {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${contentSection}'
    }
  }, // HTML?
  'article:tag':             {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${contentTags.join(", ")}'
    }
  }, // HTML?
  'article:published_time':  {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${contentPublishedAt}'
    }
  },
  'article:modified_time':   {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${contentModifiedAt}'
    }
  },
  'article:expiration_time': {
    tag:   'meta',
    attrs: {
      property: '!',
      content:  '${contentExpiresAt}'
    }
  }
};

// Common defaults
export const SEO_META_DEFAULTS: SeoContext = {
  robots:   'index, follow',
  viewport: 'width=device-width,initial-scale=1.0'
};
