import { SeoConfig, SeoContext, SeoMetaMapping, SeoMetaTag, SeoUrlContextMapper } from './seo.types';
import { SEO_META_DEFAULTS, SEO_META_MAPPING_DEFAULT }              from './seo.defaults';

const META_REF_REGEX = /\${(.*?)(?:.join\(.*?\))*?}/g;

interface MetaRef extends SeoMetaTag {
  el?: HTMLElement;
}

export class SeoManager {
  // Quick reference to get `SeoMetaTag` + DOM element (`MetaRef`) that are driven from a `SeoContext` key
  //   For example `SeoContext`.title drives `SeoMetaMapping`.{title, og:title, twitter:title}
  private configToTags:              { [configKey: string]: MetaRef[] } = {};
  private readonly document:         Document;
  private readonly headEl:           HTMLHeadElement;
  private readonly hrefAsCanonical?: boolean;
  private readonly defaults:         SeoContext;
  private readonly urlMapping:       SeoUrlContextMapper
  private readonly titleHandler?:    (title: string) => void
  private readonly metaHandler?:     (attrs: {[attr: string]: string}) => void
  private context:                   SeoContext = {};

  constructor({
    defaults            = SEO_META_DEFAULTS,
    mappings            = SEO_META_MAPPING_DEFAULT,
    urlMapping          = {},
    hrefAsCanonical     = true,
    fallbackTitle       = true,
    fallbackDescription = true,
    fallbackIcon        = true,
    fallbackRobots      = true,
    fallbackKeywords    = true,
    fallbackAuthor      = true,
    fallbackCopyright   = true,
    titleHandler,
    metaHandler,
    document
  }: SeoConfig = {}) {
    this.hrefAsCanonical = hrefAsCanonical;
    this.document        = document || window.document;
    this.headEl          = this.document.head;
    this.titleHandler    = titleHandler;
    this.metaHandler     = metaHandler;
    this.urlMapping      = urlMapping;
    this.parseMappings(mappings);
    this.defaults   = this.processDefaults({
      fallbackTitle,
      fallbackDescription,
      fallbackIcon,
      fallbackRobots,
      fallbackKeywords,
      fallbackAuthor,
      fallbackCopyright,
    }, defaults);
    this.update(this.context);
  }

  deepCopy(obj: any): any {
    if (null == obj || "object" != typeof obj) return obj;

    let copy: any = {};
    for (let attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = this.deepCopy(obj[attr]);
    }

    return copy;
  }

  // Process the `SeoMetaMapping` into two quick reference structures: metaToTags and configToTags
  private parseMappings(mappings: SeoMetaMapping): void {
    for (const [seoKey, metaConfig] of Object.entries(mappings)) {
      // Take a copy to leave original unharmed and use extended object to reference DOM element
      const metaRef: MetaRef = this.deepCopy(metaConfig);

      // Find values that reference the SEO `SeoContext` context object - i.e. `${title}`
      const toTest = [];
      if (metaRef.attrs) {
        Object.entries(metaRef.attrs).reduce((arr: string[], [attr, value]) => {
          if (value === '!') {
            // Resolve metamap key reference
            (metaRef.attrs as any)[attr] = seoKey;
          } else if (value.includes('${')) {
            toTest.push(value);
          }
          return arr;
        }, []);
      }
      if (metaRef.innerText && metaRef.innerText.includes('${')) {
        toTest.push(metaRef.innerText);
      }

      // Get referenced `SeoContext` keys to build the driver 'configToTags' mapping
      const drivers = toTest.reduce((arr: string[], value) => {
        let m = META_REF_REGEX.exec(value);

        if (m !== null) {
          while (m !== null) {
            // Catch empty match just in case
            // if (m.index === META_REF_REGEX.lastIndex) {
            //   META_REF_REGEX.lastIndex++;
            // }

            arr.push(m[1]);
            m = META_REF_REGEX.exec(value);
          }
        }
        return arr;
      }, []);

      if (drivers.length) {
        metaRef.el = this.findElement(metaRef);

        // Create a map from config key to elements they drive - title drives title, og:title, twitter:title, etc..
        drivers.forEach(configKey => {
          this.configToTags[configKey] = this.configToTags[configKey] || [];
          this.configToTags[configKey].push(metaRef);
        });

      } else {
        // If there's no drivers, the meta element is static - ensure it's in the DOM and forget about it
        this.updateElement(metaRef);
      }
    }
  }

  // Helper to sift `configToTags`'s elements and retrieve an attribute value
  private scrapeAttribute(refs: MetaRef[], attr: string): string {
    return refs
      .map(ref => ref.el?.getAttribute(attr)) // Get attribute value
      .filter(val => !!val) // Filter out if not found, empty, undefined, etc.
      [0] as string; // Should only ever be one - if not found, undefined
  }

  // Attempt to pull DOM fallback defaults if turned on and return a merged object with global defaults
  private processDefaults(config: SeoConfig, defaults: SeoContext): SeoContext {
    const fallbacks: SeoContext = {};

    // If fallback turned on and default not provided:
    //   Attempt to get current DOM value as default
    if (config.fallbackTitle && !defaults?.title && this.configToTags.title) {
      fallbacks.title = this.configToTags.title.find(ref => ref.el?.innerText)?.el?.innerText;
    }
    if (config.fallbackDescription && !defaults?.description && this.configToTags.description) {
      fallbacks.description = this.scrapeAttribute(this.configToTags.description, 'content');
    }
    if (config.fallbackIcon && !defaults?.icon && this.configToTags.icon) {
      fallbacks.icon = this.scrapeAttribute(this.configToTags.icon, 'href');
    }
    if (config.fallbackRobots && !defaults?.robots && this.configToTags.robots) {
      fallbacks.robots = this.scrapeAttribute(this.configToTags.robots, 'content');
    }
    if (config.fallbackKeywords && !defaults?.keywords && this.configToTags.keywords) {
      const str = this.scrapeAttribute(this.configToTags.keywords, 'content');
      if (str) {
        fallbacks.keywords = str.split(', ');
      }
    }
    if (config.fallbackAuthor && !defaults?.author && this.configToTags.author) {
      fallbacks.author = this.scrapeAttribute(this.configToTags.author, 'content');
    }
    if (config.fallbackCopyright && !defaults?.copyright && this.configToTags.copyright) {
      fallbacks.copyright = this.scrapeAttribute(this.configToTags.copyright, 'content');
    }

    // Cleanup just in case
    Object.keys(fallbacks)
      .filter(k => !fallbacks[k])
      .forEach(k => delete fallbacks[k]);

    return Object.assign({}, fallbacks, defaults || {});
  }

  // Safe method to get existing, find existing, or create needed meta element
  private resolveElement(ref: MetaRef): HTMLElement {
    if (ref.el) {
      return ref.el;
    }

    // const fromDom = this.findElement(ref);
    // if (fromDom) {
    //   return fromDom;
    // }

    return ref.el = this.document.createElement(ref.tag);
  }

  // Query DOM for element by tag and static attributes
  private findElement(ref: MetaRef): HTMLElement | undefined {
    const selectors = [ref.tag];

    // Add attributes without templated values to scope elements from the DOM
    if (ref.attrs) {
      for (const [attr, value] of Object.entries(ref.attrs)) {
        if (!value.includes('${')) {
          selectors.push(`[${ attr }="${ value }"]`);
        }
      }
    }

    return this.headEl.querySelector(selectors.join('')) as any;
  }

  // Replace ${var} with this.context.var
  private stringTemplate(value: string): string {
    return new Function('return `' + value.replace(/\${(.*?)}/g, '${this.$1 || ""}') + '`;').call(this.context);
  }

  // Find Element, resolve attribute values, and add to DOM if not already
  private updateElement(ref: MetaRef): void {
    let innerText = ref.innerText ? this.stringTemplate(ref.innerText) : '';
    let attrs     = Object.assign({}, ref.attrs);
    Object.keys(attrs).forEach(a => attrs[a] = this.stringTemplate(attrs[a]));

    // Delegate to handler if provided
    if (this.titleHandler && ref.tag === 'title') {
      return this.titleHandler(innerText)
    } else if (this.metaHandler && ref.tag === 'meta') {
      return this.metaHandler(attrs);
    }

    ref.el = this.resolveElement(ref);

    Object.entries(attrs).forEach(([attr, value]) => ref.el?.setAttribute(attr, this.stringTemplate(value)));
    ref.el.innerText = this.stringTemplate(innerText);

    if (!ref.el.isConnected) {
      // Add to <head> if not already in DOM
      this.headEl.appendChild(ref.el);
    }
  }

  // Update SEO context - add/update/remove elements as needed
  public update(context?: SeoContext): void {
    // Save off current SEO for comparison
    const oldSeo = this.context;

    // Merge defaults, matching URL context, and provided context
    this.context = Object.assign({}, this.defaults, this.urlMapping[this.document.location.pathname] || {}, context || {});

    // If fallback canonical and no canonical set, use current href
    if (this.hrefAsCanonical && !this.context.canonical) {
      this.context.canonical = this.document.location.href.replace(this.document.location.search, '');
    }

    // Update or remove existing
    for (const [configKey, oldValue] of Object.entries(oldSeo)) {
      if (this.context[configKey]) {
        // Do nothing if value is already correct
        if (oldValue !== this.context[configKey]) {
          this.configToTags[configKey].forEach(ref => this.updateElement(ref));
        }
      } else {
        // Remove from DOM, but keep element in memory
        this.configToTags[configKey].forEach(ref => ref.el?.remove());
      }
    }

    // Add new
    Object.keys(this.context)
      .filter(configKey => !oldSeo[configKey])
      .map(configKey => this.configToTags[configKey] || [])
      .forEach(refs => refs.forEach(ref => this.updateElement(ref)));
  }
}
