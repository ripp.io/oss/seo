import { SeoManager } from '@ripp/seo';

describe('SeoManager', () => {
  beforeEach(() => {
    while (document.head.firstChild) {
      document.head.removeChild(document.head.firstChild);
    }
  })

  it('should construct and place defaults', () => {
    let seoManager = new SeoManager();
    expect(seoManager).toBeTruthy();

    // Ref map was established
    expect(Object.keys(seoManager['configToTags'])).toContain('title');

    // Internals were established
    expect(seoManager['headEl']).toBeTruthy();
    expect(seoManager['hrefAsCanonical']).toBeTruthy();
    expect(seoManager['defaults']).toBeTruthy();

    // Context is initialized with defaults and canonical
    expect(Object.keys(seoManager['context']).length).toEqual(Object.keys(seoManager['defaults']).length + 1);
    expect(seoManager['context'].canonical).toBeTruthy();

    // Expect Default elements
    expect(Object.values(seoManager['configToTags']).reduce((count, metaRefs) => {
      metaRefs.forEach(r => {
        if (r.el) { count++ }
      });
      return count;
    }, 0)).toEqual(document.head.children.length);
    expect(document.querySelector('meta[name=robots]')).toBeTruthy();
    expect(document.querySelector('meta[name=viewport]')).toBeTruthy();
    expect(document.querySelector('link[rel=canonical]')).toBeTruthy();
    expect(document.querySelector('meta[property="og:url"]')).toBeTruthy();
    expect(document.querySelector('meta[property="twitter:url"]')).toBeTruthy();
  });

  it('should accept configuration overrides', () => {
    let testValue  = 'test';
    let seoManager = new SeoManager(
      {
        defaults: {
          testValue: testValue
        },
        mappings: {
          test1: {
            tag:       'title',
            innerText: '${testValue}'
          },
          test2: {
            tag:   'meta',
            attrs: {
              name:    '!',
              content: '${testValue}'
            }
          }
        },
        urlMapping: {
          "/test": {
            testValue: 'url-provided'
          }
        },
        document,
        hrefAsCanonical:     false,
        fallbackTitle:       false,
        fallbackDescription: false,
        fallbackIcon:        false,
        fallbackRobots:      false,
        fallbackKeywords:    false,
        fallbackAuthor:      false,
        fallbackCopyright:   false
      },
    );
    expect(seoManager).toBeTruthy();

    // Internals were established
    expect(seoManager['headEl']).toBeTruthy();
    expect(seoManager['hrefAsCanonical']).toBeFalse();
    expect(seoManager['defaults']).toBeTruthy();

    // Context is initialized with defaults (without canonical)
    expect(Object.keys(seoManager['context']).length).toEqual(Object.keys(seoManager['defaults']).length);
    expect(seoManager['context'].canonical).toBeFalsy();

    // Expect Default elements
    expect(Object.values(seoManager['configToTags']).reduce((count, metaRefs) => {
      metaRefs.forEach(r => {
        if (r.el) { count++ }
      });
      return count;
    }, 0)).toEqual(2);
    expect(document.querySelector('meta[name=robots]')).toBeFalsy();
    // expect(document.querySelector('meta[name=viewport]')).toBeFalsy();
    expect(document.querySelector('link[rel=canonical]')).toBeFalsy();
    expect(document.querySelector('meta[property="og:url"]')).toBeFalsy();
    expect(document.querySelector('meta[property="twitter:url"]')).toBeFalsy();

    expect(document.querySelector('title')).toBeTruthy();
    expect(document.querySelector('title')?.innerText).toEqual(testValue);
    expect(document.querySelector('meta[name=test2]')).toBeTruthy();
    expect(document.querySelector('meta[name=test2]')?.getAttribute('content')).toEqual(testValue);
  })

  it('should accept title and meta handlers for framework integration', () => {
    let titleCalled = false;
    let metaCalled = false;
    let seoManager = new SeoManager({
      defaults: {},
      fallbackTitle: false,
      fallbackKeywords: false,
      hrefAsCanonical: false,
      titleHandler: title => titleCalled = true,
      metaHandler:  attrs => metaCalled = true,
    });

    expect(titleCalled).toBeFalse();
    expect(metaCalled).toBeFalse();
    seoManager.update({title: 'update', keywords: ['test']})
    expect(titleCalled).toBeTrue();
    expect(metaCalled).toBeTrue();
  })

  it('should scrape existing DOM elements for fallback defaults', () => {
    const testVal   = 'test';
    const title     = document.createElement('title');
    title.innerText = testVal;
    document.head.append(title);
    const description   = document.createElement('meta');
    description.name    = 'description';
    description.content = testVal;
    document.head.append(description);
    const icon = document.createElement('link');
    icon.rel   = 'icon';
    icon.type  = 'image/x-icon';
    icon.href  = testVal;
    document.head.append(icon);
    const robots   = document.createElement('meta');
    robots.name    = 'robots';
    robots.content = testVal;
    document.head.append(robots);
    const keywords   = document.createElement('meta');
    keywords.name    = 'keywords';
    keywords.content = testVal;
    document.head.append(keywords);
    const author   = document.createElement('meta');
    author.name    = 'author';
    author.content = testVal;
    document.head.append(author);
    const copyright   = document.createElement('meta');
    copyright.name    = 'copyright';
    copyright.content = testVal;
    document.head.append(copyright);

    const seoManager = new SeoManager({
      defaults: {}
    });
    expect(seoManager).toBeTruthy();

    const defaults = seoManager['defaults'];
    expect(defaults.title).toEqual(testVal);
    expect(defaults.description).toEqual(testVal);
    expect(defaults.robots).toEqual(testVal);
    expect(defaults.keywords?.join(', ')).toEqual(testVal);
    expect(defaults.author).toEqual(testVal);
    expect(defaults.copyright).toEqual(testVal);
  })

  it('should update only changed context drivers', () => {
    let testValue1 = 'test1';
    let testValue2 = 'test2';
    let seoManager = new SeoManager({
        defaults: {
          testValue1: testValue1,
          testValue2: testValue2,
        },
        mappings: {
          test1: {
            tag:   'meta',
            attrs: {
              name:    '!',
              content: '${testValue1}'
            }
          },
          test2: {
            tag:   'meta',
            attrs: {
              name:    '!',
              content: '${testValue1}'
            }
          },
          test3: {
            tag:   'meta',
            attrs: {
              name:    '!',
              content: '${testValue2}'
            }
          },
          test4: {
            tag:   'meta',
            attrs: {
              name:    '!',
              content: '${testValue3}'
            }
          },
          test5: {
            tag:   'meta',
            attrs: {
              name:    '!',
              content: testValue1
            }
          }
        },
      },
    );
    expect(seoManager).toBeTruthy();
    expect(document.querySelector('meta[name=test1]')).toBeTruthy();
    expect(document.querySelector('meta[name=test1]')?.getAttribute('content')).toEqual(testValue1);
    expect(document.querySelector('meta[name=test2]')).toBeTruthy();
    expect(document.querySelector('meta[name=test2]')?.getAttribute('content')).toEqual(testValue1);
    expect(document.querySelector('meta[name=test3]')).toBeTruthy();
    expect(document.querySelector('meta[name=test3]')?.getAttribute('content')).toEqual(testValue2);
    expect(document.querySelector('meta[name=test4]')).toBeFalsy();
    // Static meta is thrown in DOM
    expect(document.querySelector('meta[name=test5]')).toBeTruthy();
    expect(document.querySelector('meta[name=test5]')?.getAttribute('content')).toEqual(testValue1);

    // Perform update to testValue1, which affects test1 and test2, but not test3 or test4
    let updatedValue = 'updated';
    seoManager.update({ testValue1: updatedValue })
    expect(document.querySelector('meta[name=test1]')).toBeTruthy();
    expect(document.querySelector('meta[name=test1]')?.getAttribute('content')).toEqual(updatedValue);
    expect(document.querySelector('meta[name=test2]')).toBeTruthy();
    expect(document.querySelector('meta[name=test2]')?.getAttribute('content')).toEqual(updatedValue);
    expect(document.querySelector('meta[name=test3]')).toBeTruthy();
    expect(document.querySelector('meta[name=test3]')?.getAttribute('content')).toEqual(testValue2);
    expect(document.querySelector('meta[name=test4]')).toBeFalsy();

    // Update testValue3 to add test4 to DOM, test1+test2 should revert to their default
    seoManager.update({ testValue3: updatedValue })
    expect(document.querySelector('meta[name=test1]')?.getAttribute('content')).toEqual(testValue1);
    expect(document.querySelector('meta[name=test2]')).toBeTruthy();
    expect(document.querySelector('meta[name=test2]')?.getAttribute('content')).toEqual(testValue1);
    expect(document.querySelector('meta[name=test3]')).toBeTruthy();
    expect(document.querySelector('meta[name=test3]')?.getAttribute('content')).toEqual(testValue2);
    expect(document.querySelector('meta[name=test4]')).toBeTruthy();
    expect(document.querySelector('meta[name=test4]')?.getAttribute('content')).toEqual(updatedValue);

    // Perform update to testValue1 again, test4 should be removed from DOM
    seoManager.update({ testValue1: updatedValue })
    expect(document.querySelector('meta[name=test1]')).toBeTruthy();
    expect(document.querySelector('meta[name=test1]')?.getAttribute('content')).toEqual(updatedValue);
    expect(document.querySelector('meta[name=test2]')).toBeTruthy();
    expect(document.querySelector('meta[name=test2]')?.getAttribute('content')).toEqual(updatedValue);
    expect(document.querySelector('meta[name=test3]')).toBeTruthy();
    expect(document.querySelector('meta[name=test3]')?.getAttribute('content')).toEqual(testValue2);
    expect(document.querySelector('meta[name=test4]')).toBeFalsy();
  })

  describe('DOCUMENTATION', () => {
    it('produce default mapping markdown table', () => {
      const seoManager = new SeoManager();

      const markdown = Object.entries(seoManager['configToTags'])
        .reduce((table, [driver, refs]) => {
          let elements = refs
            .map(r => `\`<${ r.tag }${ Object.entries(r.attrs || {} as { [key: string]: string })
              .map(([attr, val]) => ` ${ attr }=${ val.includes('${') ? '$' : val }`).join('') }>${ (r.innerText?.includes('${') ? '$' : r.innerText) || '' }</${ r.tag }>\``)
            .join('<br>');
          return table += `| ${ driver } | ${ elements } |\n`;
        }, '\n| Context (Driver) | SEO Element(s) Affected |\n| :--- | :--- |\n');

      console.log(markdown);
    })
  })
});
