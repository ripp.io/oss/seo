import { Rule, SchematicContext, Tree } from '@angular-devkit/schematics';
import { NodePackageInstallTask }       from '@angular-devkit/schematics/tasks';
import { Schema }                       from './Schema';
import { getWorkspace }                 from '@schematics/angular/utility/workspace';
import {
  addModuleImportToRootModule,
  getAppModulePath,
  getProjectFromWorkspace,
  getProjectMainFile,
  hasNgModuleImport
}                                       from '@angular/cdk/schematics';

const moduleSrc  = '@ripp/seo/ng';
const moduleName = 'SeoModule';

export function ngAdd(options: Schema): Rule {
  return async (tree: Tree, context: SchematicContext) => {
    context.addTask(new NodePackageInstallTask());

    if (options.registerModule) {
      const workspace     = await getWorkspace(tree);
      const project       = getProjectFromWorkspace(workspace);
      const appModulePath = getAppModulePath(tree, getProjectMainFile(project));

      if (hasNgModuleImport(tree, appModulePath, moduleName)) {
        context.logger.info('SeoModule already registered');
        return;
      }

      addModuleImportToRootModule(tree, moduleName, moduleSrc, project);
    }
  };
}
