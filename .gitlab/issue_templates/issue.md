## Summary

(Summarize the issue concisely)

## Steps to reproduce

(Simplified steps reproduce - very important for debugging)

## Example Project

(If possible, please create an example project and link to it here)

## What is the expected correct behavior?

(Expected functionality)

## Relevant logs and/or screenshots

(Console output, logs, and/or code - use code blocks (```))

## Possible fixes

(Link to the line of code that might be responsible or outline what you think should happen)

/label ~bug ~reproduced ~needs-investigation
/cc @project-manager
